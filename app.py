import os

from flask import Flask, jsonify, Response, request
from datetime import datetime
import json
from functools import wraps

app = Flask(__name__)


@app.route('/')
def root():
    return jsonify({'hello': ''})


@app.route('/api/success_req/<value>', methods=['GET'])
def success_req(value):
    return jsonify({'value': value})


@app.route('/api/time')
def time():
    return jsonify({'time': datetime.now()})


@app.route('/api/unauth/<user>')
def unauth(user):
    return Response(status=401,
                    mimetype="application/json",
                    response=json.dumps({'user': user}))


def check_auth(username, password):
    return username == 'admin' and password == 'password'


def requires_auth(f):
    @wraps(f)
    def decorated(*args, **kwargs):
        auth = request.authorization

        if not auth or not check_auth(auth.username, auth.password):
            return Response('Not authorized', 401, {'WWW-Authenticate': 'Basic'})

        return f(*args, **kwargs)

    return decorated


@app.route('/api/auth')
@requires_auth
def auth():
    return 'auth!'


@app.route('/api/response/<str>/<int:code>')
@app.route('/api/response/<str>')
@requires_auth
def response_with_code(str=None, code=None):
    resp = Response()

    if str is not None:
        resp.response = str

    if code is not None:
        resp.status_code = code

    return resp


@app.route('/quests')
def quests():
    return jsonify([
        {'id': 1, 'title': 'Quest 1', 'image': 'http://putin24.info/upload/editor/image/kvest_otlichnyj_otdyh.jpg'},
        {'id': 2, 'title': 'Quest 2', 'image': 'http://www-images.theonering.org/torwp/wp-content/uploads/2014/05/TheQuest.jpg'},
        {'id': 3, 'title': 'Quest 3', 'image': 'http://www.avcrf.ru/media-files/events/%D0%BA%D0%B2%D0%B5%D1%81%D1%82.jpg'},
        {'id': 4, 'title': 'Quest 4', 'image': 'http://www.fotonastenu.ru/images/joomgallery/thumbnails/_29/28018_20160803_1714209011.jpg'},
    ])


@app.route('/quest/<int:id>')
def quest(id):
    data = {
        1: {'id': 1, 'title': 'Quest 1', 'image': 'http://putin24.info/upload/editor/image/kvest_otlichnyj_otdyh.jpg'},
        2: {'id': 2, 'title': 'Quest 2', 'image': 'http://www-images.theonering.org/torwp/wp-content/uploads/2014/05/TheQuest.jpg'},
        3: {'id': 3, 'title': 'Quest 3', 'image': 'http://www.avcrf.ru/media-files/events/%D0%BA%D0%B2%D0%B5%D1%81%D1%82.jpg'}
    }.get(id, None)

    return jsonify(data)


@app.route('/rating')
def rating():
    arr = [{'nickname': 'Nickname {}'.format(x)} for x in range(21)]
    return jsonify(arr)


if __name__ == '__main__':
    port = int(os.environ.get('PORT', 8000))
    app.run(host='0.0.0.0', port=port, debug=True)
